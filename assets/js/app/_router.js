module.exports = function($stateProvider, $urlRouterProvider, configProvider) {

    $urlRouterProvider.otherwise('/00-00-title');

    angular.forEach(configProvider.config.slides, function(slide) {
        $stateProvider.state(slide.uid, {
            url: '/' + slide.uid,
            templateUrl: 'views/' + slide.uid + '.html',
            controller: 'slideController'
        });
    });
};
