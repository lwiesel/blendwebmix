module.exports = function($scope, navigator){
    // Slide numbers
    $scope.position = function() {
        return navigator.getPosition();
    };
    $scope.slideName = function() {
        return navigator.getSlideName();
    };
    $scope.slides = function() {
        return navigator.getSlides();
    };
    $scope.slidesNumber = navigator.getSlidesNumber();

    $scope.partialsStates = {
        'title': 'title',
        'slide': 'slide',
        'slide-2': 'slide-2'
    };

    // Animation
    $scope.direction = null;

    // Prev / next actions
    $scope.previous = function() {
        $scope.direction = 'backward';
        navigator.previous();
    };
    $scope.next = function() {
        $scope.direction = 'forward';
        navigator.next();
    };
    $scope.requestSlide = function(requestedPosition) {
        if (requestedPosition > 0 && requestedPosition <= $scope.slidesNumber) {
            $scope.direction = 'up';
            navigator.goToPosition(requestedPosition);
        } else {
            $scope.requestedPosition = $scope.position();
        }
    };
    $scope.requestSlideName = function(slideName) {
        if (navigator.getSlideUids().indexOf(slideName) > -1) {
            $scope.direction = 'up';
            navigator.goTo(slideName);
        }
    };

    $scope.$watch($scope.position, function() {
        $scope.requestedPosition = $scope.position();
    });

    // EVENT LISTENERS
    $scope.$on('REQUEST_PREVIOUS_SLIDE', function() {
        $scope.partialsState = "visible";
        $scope.previous();
    });

    $scope.$on('REQUEST_NEXT_SLIDE', function() {
        $scope.partialsState = "hidden";
        $scope.next();
    });
};
