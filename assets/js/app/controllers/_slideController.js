module.exports = function($scope, navigator, hotkeys) {
    /** -------------------------------------
    PARTIALS
    ------------------------------------- **/
    var partials, canSlidePrev, canSlideNext;

    var updatePartials = function(index) {
        partials = document.getElementsByClassName('partial');
        angular.forEach(partials, function(partial, index) {
            if (index <= navigator.getCurrentPartialPosition()) {
                angular.element(partial).addClass('active');
                toggleSwitch(partial, false);
            } else {
                angular.element(partial).removeClass('active');
                toggleSwitch(partial, true);
            }
        });
    };

    var toggleSwitch = function(element, show) {
        if (
            typeof(element.dataset) == 'undefined' ||
            typeof(element.dataset.switch) == 'undefined'
        ) {
            return false;
        }

        var targetElement = document.querySelector(element.dataset.switch);

        if (show) {
            angular.element(targetElement).removeClass('switched');
        } else {
            angular.element(targetElement).addClass('switched');
        }
    };

    var checkPartials = function() {
        partials = document.getElementsByClassName('partial');
        canSlidePrev = (navigator.getCurrentPartialPosition() + 1) === 0;
        canSlideNext = (partials.length - (navigator.getCurrentPartialPosition() + 1)) === 0;
    };

    /** -------------------------------------
    HIDDEN LINES
    ------------------------------------- **/
    $scope.isLinesHidden = true;
    $scope.linesHiddenClass = function() {
        return $scope.isLinesHidden ? 'lines-hidden' : '';
    };

    /** -------------------------------------
    LAUNCH ON FIRST SIGHT
    ------------------------------------- **/
    updatePartials();

    /** -------------------------------------
    HOTKEYS
    ------------------------------------- **/
    hotkeys.add({
        combo: 'left',
        description: 'Go to the previous slide',
        callback: function() {
            checkPartials();

            if (canSlidePrev) {
                $scope.$emit('REQUEST_PREVIOUS_SLIDE');
            } else {
                navigator.previousPartial();
                updatePartials();
            }
        }
    });
    hotkeys.add({
        combo: 'right',
        description: 'Go to the next slide',
        callback: function() {
            checkPartials();

            if (canSlideNext) {
                $scope.$emit('REQUEST_NEXT_SLIDE');
            } else {
                navigator.nextPartial();
                updatePartials();
            }
        }
    });
    hotkeys.add({
        combo: 'c',
        description: 'Toggle line highlight',
        callback: function() {
            $scope.isLinesHidden = !$scope.isLinesHidden;
        }
    });
};
