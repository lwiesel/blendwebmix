module.exports = function() {
    this.config = {
        slides: [
            {uid: '00-00-title', title: 'Titre'},
            {uid: '00-01-me', title: 'About me'},
            {uid: '10-00-smelly-code', title: '0. The Smelly Code'},
            {uid: '10-01-code', title: '0.1. Meet the Smelly Code'},
            {uid: '20-00-ptow', title: '~ Plan to take over the world'},
            {uid: '30-00-oocss', title: '1. OOCSS'},
            {uid: '30-01-definition', title: '1.1. Definition'},
            {uid: '30-02-wtf', title: '1.1. #WTF'},
            {uid: '30-02-code', title: '1.2. Code'},
            {uid: '30-03-ptow', title: '~ Plan to take over the world'},
            {uid: '40-00-preprocessors', title: '2. Les pré-processeurs'},
            {uid: '40-01-definition', title: '2.1. Définition'},
            {uid: '40-02-code', title: '2.3. Code'},
            {uid: '40-03-ptow', title: '~ Plan to take over the world'},
            {uid: '50-00-class-naming', title: '3. La nomenclature de classes'},
            {uid: '50-01-quotation', title: '3.1. Citation'},
            {uid: '50-02-bem', title: '3.2. La nomenclature BEM'},
            {uid: '50-03-code', title: '3.3. Code'},
            {uid: '50-04-other-namings', title: '3.4. Autres nomenclatures'},
            {uid: '50-05-ptow', title: '~ Plan to take over the world'},
            {uid: '60-00-atomic-design', title: '4. L\'atomic Design'},
            {uid: '60-01-quotation', title: '4.1. Citation'},
            {uid: '60-02-atomic-categories', title: '4.2. Categories'},
            {uid: '60-03-schema-light', title: '4.3. Schéma light'},
            {uid: '60-04-schema-full', title: '4.4. Schéma complet'},
            {uid: '60-05-revelation', title: '4.5. Révélation !'},
            {uid: '60-06-give-up', title: '4.6. #give-up'},
            {uid: '60-07-schema-meta-full', title: '4.7. Schéma meta complet'},
            {uid: '60-08-ptow', title: '~ Plan to take over the world'},
            {uid: '70-00-webcomponents', title: '5. Les Webcomponents'},
            {uid: '70-01-definition', title: '5.1. Définition'},
            {uid: '70-02-docs', title: '5.2. Docs'},
            {uid: '70-03-ptow', title: '~ Plan to take over the world'},
            {uid: '80-00-cliffhanger', title: 'Cliffhanger'},
            {uid: '80-01-questions', title: 'Questions ?'},
            {uid: '90-00-final-me', title: 'Final me'},
        ]
    };

    this.$get = function() { return this.config; };
};
