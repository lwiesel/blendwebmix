(function () {
    'use strict';

    var angular = require('angular');
    require('angular-hotkeys');

    angular
        .module('app', [
            require('angular-ui-router'),
            require('angular-animate'),
            'cfp.hotkeys'
        ])
        .provider('config', require('./configs/_config'))
        .factory('navigator', ['config', '$state', require('./services/_navigator')])
        .controller('mainController', ['$scope', 'navigator', require('./controllers/_mainController')])
        .controller('slideController', ['$scope', 'navigator', 'hotkeys', require('./controllers/_slideController')])
        .config(['$stateProvider', '$urlRouterProvider', 'configProvider', require('./_router')])
        .directive('bgImage', require('./directives/_bgImage'))
        .directive('prism', require('./directives/_prism.js'))
        .directive('checkbox', require('./directives/_checkbox.js'))
        .directive('author', require('./directives/_author.js'))
    ;
}());
