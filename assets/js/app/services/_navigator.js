module.exports = function(config, $state) {
    var slideStates = {};

    angular.forEach(config.slides, function(slide, index) {
        slideStates[slide.uid] = -1;
    });

    return {
        _toSlideIndex: function(slideName) {
            resultIndex = -1;

            angular.forEach(config.slides, function(slide, index) {
                if (slideName == slide.uid) {
                    resultIndex = index;
                }
            });

            return resultIndex;
        },
        _getCurrentSlideIndex: function() {
            return this._toSlideIndex($state.current.name);
        },
        _getCurrentSlideName: function() {
            return $state.current.name;
        },
        previous: function() {
            var currentSlideIndex = this._getCurrentSlideIndex();

            if (currentSlideIndex > 0) {
                this.goTo(config.slides[currentSlideIndex - 1].uid);
            }
        },
        next: function() {
            var currentSlideIndex = this._getCurrentSlideIndex();

            if (currentSlideIndex < config.slides.length - 1) {
                this.goTo(config.slides[currentSlideIndex + 1].uid);
            }
        },
        goTo: function(slideName) {
            if (this._toSlideIndex(slideName) > -1) {
                $state.go(slideName);
            } else {
                throw 'Unknown slide ' + slideName;
            }
        },
        goToPosition: function(slidePosition) {
            if (slidePosition > 0 && slidePosition <= this.getSlidesNumber()) {
                $state.go(config.slides[slidePosition - 1].uid);
            } else {
                throw 'Unvalid slide number ' + slidePosition;
            }
        },
        getPosition: function() {
            return (this._getCurrentSlideIndex() + 1);
        },
        getSlidesNumber: function() {
            return config.slides.length;
        },
        getSlides: function() {
            return config.slides;
        },
        getSlideName: function() {
            return this._getCurrentSlideName();
        },
        getSlideUids: function() {
            var slideUids = [];

            angular.forEach(config.slides, function(slide) {
                slideUids.push(slide.uid);
            });

            return slideUids;
        },
        _slideStates: slideStates,
        nextPartial: function() {
            this._slideStates[this._getCurrentSlideName()]++;
        },
        previousPartial: function() {
            this._slideStates[this._getCurrentSlideName()] = window.Math.max(this._slideStates[this._getCurrentSlideName()] - 1, -1);
        },
        getCurrentPartialPosition: function() {
            return this._slideStates[this._getCurrentSlideName()];
        }
    };
};
