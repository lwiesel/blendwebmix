require('prismjs');
require('prismjs/plugins/line-highlight/prism-line-highlight');
require('prismjs/plugins/line-numbers/prism-line-numbers');

module.exports = function(){
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            langage: '@',
            line: '@',
            numbers: '@',
            label: '@'
        },
        template: '<pre data-line="{{ line }}" class="{{ lineNumbers }}"><div class="prism-label">{{ label }}</div><code class="language-{{langage}}" ng-transclude></code></pre>',
        link: function ($scope, element, attrs) {
            $scope.lineNumbers = attrs.numbers == 'true' ? 'line-numbers' : '';

            element.ready(function() {
                Prism.highlightElement(element[0].querySelector('code'));
            });
        }
    };
};
