module.exports = function(){
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            checked: '@',
            identifier: '@'
        },
        template: '<span id="{{identifier}}" class="checkbox-custom" ng-click="check()"><input ng-if="checked" type="checkbox" checked="checked"><input ng-if="!checked" type="checkbox"><label ng-transclude></label><svg ng-if="checked" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><path d="M16.667,62.167c3.109,5.55,7.217,10.591,10.926,15.75 c2.614,3.636,5.149,7.519,8.161,10.853c-0.046-0.051,1.959,2.414,2.692,2.343c0.895-0.088,6.958-8.511,6.014-7.3 c5.997-7.695,11.68-15.463,16.931-23.696c6.393-10.025,12.235-20.373,18.104-30.707C82.004,24.988,84.802,20.601,87,16" style="stroke-dasharray: 126.37px, 126.37px; stroke-dashoffset: 0px;"></path></svg></span>',
        link: function ($scope, element, attrs) {
            $scope.checked = attrs.checked && attrs.checked;
            $scope.identifier = attrs.identifier;

            $scope.check = function() {
                $scope.checked = !$scope.checked;
            };
        }
    };
};
