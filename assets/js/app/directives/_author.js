module.exports = function() {
    return {
        restrict: 'E',
        replace: true,
        transclude: false,
        scope: {},
        template: '<div style="position: absolute;bottom: 1em;right: 0;padding: 1em 0 1em 1em;"><div style="text-align: right;float: right;padding: 0.5em 1em;font-size: 1.2em;"><div style="display: flex;flex-direction: row;justify-content: space-between;align-items: baseline"><i class="fa fa-twitter" style="color: #00aced;margin-right: 1em;"></i><div style="text-align: right"><a href="https://twitter.com/LWiesel" style="font-weight: bold">@LWiesel</a><br></div></div><div style="display: flex;flex-direction: row;justify-content: space-between;align-items: baseline"><i class="fa fa-linkedin" style="color: #007bb5;margin-right: 1em;"></i><div style="text-align: right"><a href="https://fr.linkedin.com/in/lwiesel" style="font-weight: bold">lwiesel</a><br></div> </div></div></div>',
        link: function($scope, element, attrs) {

        }
    }
};
