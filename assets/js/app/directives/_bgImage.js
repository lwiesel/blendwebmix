module.exports = function(){
    return function(scope, element, attrs){
        attrs.$observe('bgImage', function(value) {
            var bgRepeat = 'repeat';
            var bgSize = 'cover';

            if("bgRepeat" in attrs) {
                bgRepeat = (attrs.bgRepeat !== undefined && attrs.bgRepeat !== '') ? attrs.bgRepeat : 'repeat';
            }

            if("bgSize" in attrs) {
                bgSize = (attrs.bgSize !== undefined && attrs.bgSize !== '') ? attrs.bgSize : 'cover';
            }

            element.css({
                'background-image': 'url("' + value +'")',
                'background-repeat': bgRepeat,
                'background-size': bgSize,
                'background-position': 'center center'
            });
        });
    };
};
