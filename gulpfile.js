var gulp = require('gulp');

var browserify      = require('browserify');
var del             = require('del');
var gulpif          = require('gulp-if');
var imagemin        = require('gulp-imagemin');
var jshint          = require('gulp-jshint');
var autoprefixer    = require('gulp-autoprefixer');
var sass            = require('gulp-sass');
var sourcemaps      = require('gulp-sourcemaps');
var through         = require('through2');
var uglify          = require('gulp-uglify');
var util            = require('gulp-util');

var paths = {
    js: ['assets/js/**/[!_]*.js'],
    images: ['assets/img/**/*'],
    sass: ['assets/scss/**/*.scss'],
    css: [
        'node_modules/angular-hotkeys/build/hotkeys.min.css',
        'node_modules/prismjs/themes/prism.css',
        'node_modules/prismjs/plugins/line-highlight/prism-line-highlight.css',
        'node_modules/prismjs/plugins/line-numbers/prism-line-numbers.css'
    ],
    views: ['assets/views/**/*.html'],
    webfonts: ['assets/fonts/**/*', 'node_modules/font-awesome/fonts/**/*'],
    components: ['assets/components', 'node_modules'],
    dest: 'web'
};

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use any package available on npm
gulp.task('clean', function() {
    // You can use multiple globbing patterns as you would with `gulp.src`
    return del([paths.dest + '/css', paths.dest + '/js', paths.dest + '/img', paths.dest + '/fonts']);
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src(paths.js)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('css', function() {
    return gulp.src(paths.css)
        .pipe(gulp.dest(paths.dest + '/css'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src(paths.sass)
        .pipe(gulpif(util.env.dev, sourcemaps.init()))
        .pipe(sass({
            includePaths: [].concat(paths.components),
            outputStyle: util.env.dev ? 'nested' : 'compressed',
            precision: 10
        }))
        .pipe(autoprefixer({
            browsers: ["last 2 version", "> 1%", "ie 8", "ie 7"]
        }))
        .pipe(gulpif(util.env.dev, sourcemaps.write('./')))
        .pipe(gulp.dest(paths.dest + '/css'));
});

gulp.task('js', ['lint'], function () {
    var browserified = through.obj(function(file, enc, next) {
        browserify(file.path, {
            paths: paths.components,
            debug: util.env.dev
        })
            .bundle(function(err, res) {
                // assumes file.contents is a Buffer
                file.contents = res;
                next(null, file);
            })
        ;
    });

    return gulp.src(paths.js)
        .pipe(browserified)
        .pipe(gulpif(!util.env.dev, uglify()))
        .pipe(gulp.dest(paths.dest + '/js'));
});

// Copy all static images
gulp.task('images', function() {
    return gulp.src(paths.images)
        // Pass in options to the task
        .pipe(gulpif(!util.env.dev, imagemin({optimizationLevel: 5})))
        .pipe(gulp.dest(paths.dest + '/images'));
});

// Copy all views
gulp.task('views', function() {
    return gulp.src(paths.views)
        .pipe(gulp.dest(paths.dest + '/views'));
});

// Copy all fonts
gulp.task('fonts', function() {
    return gulp.src(paths.webfonts)
        .pipe(gulp.dest(paths.dest + '/fonts'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(paths.js, ['lint', 'scripts']);
    gulp.watch(paths.images, ['images']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['lint', 'sass', 'js', 'images', 'views', 'fonts']);
