Pimp My Front
====

## Fiche personnelle
*photo*
- Développeur Full-Stack & Chef de projet technique
- Taikonaute du frontend & DevOps convaincu

## Introduction de la problématique

- meet the smelly code

## OOCSS

- principe de l'OOCSS
- application concrète
- évolution du code
- problématique de l'`import`

## Pré-processeurs

- principe du préprocesseur
- application concrète - fonctionnalités utiles ici
- évolution du code

## Nomenclature de classe

- principe des différentes nomenclatures de classes
- principe de la notation raisonnée
- présentation de BEM
- évolution du code

## Atomic design

- principe de l'atomic design
- ce que c'est / ce que ce n'est pas
- application concrète - principe des couches
- schéma light
- schéma complet
- schéma méga complet

## What's next ?

- principe des web components

## Question time (5 min.)
